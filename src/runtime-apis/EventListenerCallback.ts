import { CallEvent } from "./CallEvent";
export type EventListenerCallback = (event: CallEvent) => void;
