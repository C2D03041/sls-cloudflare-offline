import {CloudflareRequest} from "./Request";
import {CloudflareResponse} from "./Response";

export interface CallEvent {
	request: CloudflareRequest;
	respondWith: (response: CloudflareResponse | Promise<CloudflareResponse>) => void;
	// passThroughOnException
	// waitUntil: ?
}
