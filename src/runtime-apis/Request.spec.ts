import {Headers} from "node-fetch";
import {Request, RequestOptions} from './Request';
import {CfObject} from './CfObject';

describe('Request', () => {
	let instance: Request;
	let theUrl: string = "/test/url";
	let theOptions: RequestOptions = {
		method: "GET",
		headers: new Headers(),
		body: "testBody",
		redirect: "manual",
		cf: {} as any as CfObject
	};

	beforeEach(() => {
		instance = new Request(theUrl, theOptions);
	});

	it('should be defined', () => expect(instance).toBeDefined());
	it('should store the url', () => expect((instance as any).url).toBe(theUrl));
	it('should store the options', () => expect((instance as any).init).toBe(theOptions));
});
