import {Headers} from "node-fetch";
import {Response, ResponseOptions} from './Response';

describe('Response', () => {
	let instance: Response;
	let theBody: string = "the body";
	let theOptions: ResponseOptions = {
		status: 200,
		statusText: "OK",
		headers: new Headers()
	};

	beforeEach(() => {
		instance = new Response(theBody, theOptions);
	})

	it('should be defined', () => expect(instance).toBeDefined());
	it('should store the body', () => expect(instance.body).toBe(theBody));
	it('should store the options', () => expect(instance.init).toBe(theOptions));
});
