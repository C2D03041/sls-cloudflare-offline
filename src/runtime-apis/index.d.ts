import { EventListenerCallback } from "./EventListenerCallback"; // eslint-disable-line @typescript-eslint/no-unused-vars
import runtimeApi from "./index.ts";

declare global {
	declare function addEventListener(key: string, cb: EventListenerCallback);

	const Request: typeof runtimeApi.Request;
	const Response: typeof runtimeApi.Response;
}
