import {Headers} from "node-fetch";

export interface ResponseOptions {
	status: number;
	statusText: string;
	headers: Headers;
}

export class Response {
	constructor(public body: string, public init: ResponseOptions) {}
}

export type CloudflareResponse = Response;
