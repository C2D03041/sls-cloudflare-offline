import {Headers} from "node-fetch";
import { CfObject } from "./CfObject";

export interface RequestOptions {
	method: string;
	headers: Headers;
	body: string;
	redirect: "manual" | "follow";
	cf: CfObject;
}

export class Request {
	constructor(private url: string, private init?: RequestOptions) {}

	// arrayBuffer(): ArrayBuffer
	// blob(): Blob
	// formData(): FormData
	// json(): any
	// text(): USVString
}

export type CloudflareRequest = Request;
