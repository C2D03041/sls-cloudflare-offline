export interface FunctionEvent {
	http: {
		url: string;
		method: string;
	};
}
