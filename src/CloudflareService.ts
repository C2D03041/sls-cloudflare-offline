import {FunctionSpec} from './FunctionSpec';
import {Serverless} from './Serverless';

export interface CloudflareService {
	serverless: Serverless;
	service: string;
	serviceObject: {
		name: string;
		// idk if there's others
	};
	plugins: string[];
	functions: {
		[k: string]: FunctionSpec;
	};
	// resources: ?
	// package: ?
}
