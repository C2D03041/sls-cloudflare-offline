import {FunctionSpec} from './FunctionSpec';
import {EventListenerCallback} from './runtime-apis/EventListenerCallback';

export interface EventCallback {
	name: string;
	spec: FunctionSpec;
	invoke: EventListenerCallback;
}
