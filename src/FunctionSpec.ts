import {FunctionEvent} from './FunctionEvent';

export interface FunctionSpec {
	worker: string;
	script: string;
	events: FunctionEvent[];
	webpackConfig?: string;
	name: string;
}
