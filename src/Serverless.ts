import {CloudflareService} from './CloudflareService';

export interface Serverless {
	cli: {
		log(message: string): void;
	};

	service: CloudflareService;

	functions: {};
}
