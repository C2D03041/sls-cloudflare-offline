import { CloudflareResponse } from './runtime-apis/Response';
import { Response as ExpressResponse } from 'express';
import { applyCloudflareResponseToExpressResponse as applyFn } from './applyCloudflareResponseToExpressResponse';
import { DeepPartial } from 'utility-types';
import { Headers } from 'node-fetch';

let fakeCloudflareResponse: DeepPartial<CloudflareResponse> = {
	init: {
		headers: new Headers(),
		status: 404,
	},
	body: "the body",
};

let _headers = {};
let fakeExpressResponse: DeepPartial<ExpressResponse> = {
	status(status: number) { (this as any)._status = status; return this },
	send(body: string) { (this as any)._body = body; return this }
};

(fakeExpressResponse as any)._headers = {};
(fakeExpressResponse as any).set = (k: string, v: string) => (fakeExpressResponse as any)._headers[k] = v;

describe('applyCloudflareResponseToExpressResponse', () => {
	it('should apply the stored fields to the response object', () => {
		fakeCloudflareResponse.init.headers.set("k", "v");
		applyFn(fakeCloudflareResponse as CloudflareResponse, fakeExpressResponse as ExpressResponse);
		
		expect((fakeExpressResponse as any)._headers).toEqual({k: "v"});
		expect((fakeExpressResponse as any)._status).toBe(fakeCloudflareResponse.init.status);
		expect((fakeExpressResponse as any)._body).toBe(fakeCloudflareResponse.body);
	});
	it('should not throw an exception when the headers object is undefined', () => {
		fakeCloudflareResponse.init.headers = undefined;
		applyFn(fakeCloudflareResponse as CloudflareResponse, fakeExpressResponse as ExpressResponse);
	});
	it('should not throw an exception when the init object is undefined', () => {
		fakeCloudflareResponse.init = undefined;
		applyFn(fakeCloudflareResponse as CloudflareResponse, fakeExpressResponse as ExpressResponse);
	});
});
