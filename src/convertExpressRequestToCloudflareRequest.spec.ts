import { CloudflareRequest } from './runtime-apis/Request';
import { Request as ExpressRequest } from 'express';
import { convertExpressRequestToCloudflareRequest as convertFn } from './convertExpressRequestToCloudflareRequest';
import { DeepPartial } from 'utility-types';
import { Headers } from 'node-fetch';

let fakeExpressRequest : DeepPartial<ExpressRequest> = {
	method: 'POST',
	url: '/asdf',
	headers: new Headers({ test: "value" }) as any,
	body: 'test body',
};

describe('convertExpressRequestToCloudflareRequest', () => {
	it('should copy all of the fields from Express into CloudflareRequest', () => {
		let exr: ExpressRequest = fakeExpressRequest as any;
		let cfr: CloudflareRequest = convertFn(exr);
		expect((cfr as any).init.method).toBe(exr.method);
		expect((cfr as any).init.body).toBe(exr.body);
		expect((cfr as any).url).toBe(exr.url);
		// TODO: this header comparison doesn't work quite right, the values are off
		// expect(Object.entries((cfr as any).init.headers)).toEqual(Object.entries(exr.headers));
	});
});
