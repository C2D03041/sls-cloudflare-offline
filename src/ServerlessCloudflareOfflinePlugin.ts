import { promisify /*, promisifyAll*/ } from "bluebird";
import express from "express";
import isPromise from "is-promise";
import path from "path";
import _webpack, {Stats as WebpackStats} from "webpack";
import webpackMerge from "webpack-merge";
import {applyCloudflareResponseToExpressResponse} from "./applyCloudflareResponseToExpressResponse";
import {convertExpressRequestToCloudflareRequest} from "./convertExpressRequestToCloudflareRequest";
import "./runtime-apis";
// import {CloudflareRequest} from "./runtime-apis/Request";
import {CloudflareResponse} from "./runtime-apis/Response";
const webpack = promisify(_webpack);
// import _fs from "fs";
// const fs = promisifyAll(_fs);
import { CallEvent } from "./runtime-apis/CallEvent";
import { EventListenerCallback } from "./runtime-apis/EventListenerCallback";
import expressHttpProxyMiddleware from 'express-http-proxy';
import {Serverless} from './Serverless';
import {OptionValues} from './OptionValues';
import {EventCallback} from './EventCallback';

export interface OptionSpecification {
	[k: string]: {
		usage: string;
		required?: boolean;
		shortcut?: string;
		default?: any;
	};
}

export class ServerlessCloudflareOfflinePlugin {

	public require: typeof require = require;

	public commands: {
		[k: string]: {
			usage: string;
			lifecycleEvents: string[];
			options: OptionSpecification;
		};
	} = {
		offline: {
			usage: "Helps you start your first Serverless plugin",
			lifecycleEvents: [
				"start",
				"stop",
			],
			options: {
				message: {
					usage:
            "Specify the message you want to deploy "
            + '(e.g. "--message \'My Message\'" or "-m \'My Message\'")',
					required: false,
					shortcut: "m",
				},
				port: {
					usage: "Port number",
					required: false,
					shortcut: "p",
					default: 3000,
				},
				host: {
					usage: "bind host",
					required: false,
					shortcut: "h",
					default: "127.0.0.1",
				},
			},
		},
	};

	public hooks: {
		[k: string]: () => void;
	} = {
		// 'before:offline:start': this.beforeWelcome.bind(this),
		"offline:start": this.offlineStart.bind(this),
		//"offline:stop": this.offlineStop.bind(this),
		// 'after:offline:stop': this.afterHelloWorld.bind(this),
	};

	constructor(
		private serverless: Serverless,
		private options: OptionValues,
	) {}

	public async offlineStart(): Promise<void> {
		const cbs = await this.generateEventCallbacks();
		// console.log('functions', cbs);

		const app = express();
		for (const cb of cbs) {
			for (const event of cb.spec.events) {
				const url = "/" + event.http.url.split("/").slice(1).join("/");
				this.serverless.cli.log(`Mounting ${event.http.method} ${url} -> ${cb.name}`);
				app.use((req, res, next) => {
					// console.log('checking', url, '===', req.url, 'and', event.http.method, '===', req.method);
					if (url !== req.url || event.http.method !== req.method) {
						next();
						return;
					}
					const request = convertExpressRequestToCloudflareRequest(req);
					const ev: CallEvent = {
						request,
						async respondWith(response: CloudflareResponse | Promise<CloudflareResponse>) {
							if (isPromise(response)) {
								response = await response;
							}
							applyCloudflareResponseToExpressResponse(response, res);
						},
					};
		  
		  			cb.invoke(ev);
				});
			}
		}

		app.use((req, res) => {
			this.serverless.cli.log(`${req.method} ${req.url} -> sending 404`);
			res.status(404);
			res.send("404");
		});

		console.log("opts", this.options);
		await new Promise((_resolve: (value?: unknown) => void, _reject: (reason?: any) => void) => {
			app.listen(this.options.port, this.options.host, () => this.serverless.cli.log("Offline Ready"));
		});
	}

	public async generateEventCallback(name: string): Promise<EventCallback> {
		const spec = this.serverless.service.functions[name];
		let scriptPath = null;
		if (spec.webpackConfig) {
			this.serverless.cli.log(`${name} -> Parsing webpackConfig ${spec.webpackConfig}`);
			const config = this.require(path.join(process.cwd(), spec.webpackConfig)); // eslint-disable-line @typescript-eslint/no-var-requires
			const newConfig: typeof config = webpackMerge(config, {
				entry: path.join(process.cwd(), spec.script),
			});
			// console.log('config', config);
			const stats = await webpack(newConfig) as WebpackStats;
			if (stats.hasErrors()) {
				throw new Error("Webpack errors: " + stats.toString());
			}
			// console.log('stats', stats);
			console.log("outputOptions", stats.compilation.outputOptions);
			const dir = stats.compilation.outputOptions.path as string;
			const filename = stats.compilation.outputOptions.filename as string;
			scriptPath = path.join(dir, filename);
			this.serverless.cli.log(`${name} -> Webpack generated this bundle, require()ing it... ${scriptPath}`);
		} else {
			scriptPath = path.join(process.cwd(), spec.script);
			this.serverless.cli.log(`${name} -> No webpackConfig present, require()ing script directly... ${scriptPath}`);
		}
		let invoke: EventListenerCallback = null;
		(global as any).addEventListener = (event: string, cb: EventListenerCallback): void => {
			if (event !== "fetch") { return; }
			invoke = cb;
		};
		console.log('requiring', scriptPath);
		this.require(scriptPath);
		if (!invoke) {
			throw new Error("Function " + name + " did not call addEventListener");
		}
		return {
			name,
			invoke,
			spec,
		};
	}

	public async generateEventCallbacks(): Promise<EventCallback[]> {
		const names = Object.keys(this.serverless.service.functions);
		const ps = names.map(async (name) => this.generateEventCallback(name))

		return Promise.all(ps);

	}
}

