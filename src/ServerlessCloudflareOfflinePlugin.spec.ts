import {ServerlessCloudflareOfflinePlugin} from './ServerlessCloudflareOfflinePlugin';
import {Serverless} from './Serverless';
import {OptionValues} from './OptionValues';
import {FunctionSpec} from './FunctionSpec';
import {DeepPartial} from 'utility-types';
import path from 'path';

// these could technically be typed as webpack but we don't really care about checking them
let webpackResponseError: any = null;
let webpackResponseBody: any = null;
jest.mock('webpack', () => (moduleConfig: string, callback: (err: any, stats: any) => void) => {
	callback(webpackResponseError, webpackResponseBody);
});

const testFunctionName = "TEST_FN";

function generateServerless(fn: FunctionSpec): Serverless {
	let serverless: DeepPartial<Serverless> = {
		service: {
			functions: {
				[testFunctionName]: fn
			}
		},
		cli: {
			log: (msg: string) => console.log(msg)
		}
	};
	return serverless as Serverless;
}

describe('ServerlessCloudflareOfflinePlugin', () => {
	let instance: ServerlessCloudflareOfflinePlugin;

	describe('generateEventCallback', () => {
		it('should return a valid result with the webpack flag disabled', async () => {
			let func: FunctionSpec = {
				worker: "test",
				script: "test-script",
				events: [],
				name: "test"
			};
			let serverless = generateServerless(func);
			let options: DeepPartial<OptionValues> = {};
			instance = new ServerlessCloudflareOfflinePlugin(serverless as Serverless, options);
			instance.require = jest.fn(() => {
				let ev = (global as any).addEventListener;
				expect(ev).toBeDefined();
				ev("fetch", () => {})
				return {}
			}) as any;
			let result = await instance.generateEventCallback(testFunctionName);
			expect(instance.require).toHaveBeenCalledWith(
				path.join(__dirname, '..', serverless.service.functions[testFunctionName].script)
			);
		});

		let webpackConfigName = "someFile.webpack.config.js";
		it('should return a valid result with the webpack flag enabled', async () => {
			let func: FunctionSpec = Object.freeze({
				worker: "test",
				script: "test-script",
				events: [],
				name: "test",
				webpackConfig: webpackConfigName
			});
			let serverless = generateServerless(func);
			let options: DeepPartial<OptionValues> = {};
			instance = new ServerlessCloudflareOfflinePlugin(serverless as Serverless, options);
			
			webpackResponseBody = {
				hasErrors: () => false,
				compilation: {
					outputOptions: {
						path: "test/dir",
						filename: "test_bundle.js"
					}
				}
			};
			let bundleScriptName = path.join(webpackResponseBody.compilation.outputOptions.path, webpackResponseBody.compilation.outputOptions.filename);
		
			let requireFn = jest.fn((scriptName: string) => {
				if(scriptName === path.join(process.cwd(), webpackConfigName)) {
					// mock importing the config file
					return {};
				} else if(scriptName === bundleScriptName) {
					// mock executing the bundle
					let ev = (global as any).addEventListener;
					expect(ev).toBeDefined();
					ev("fetch", () => {})
					return {}
				} else {
					throw new Error("unknown required script: "+scriptName);
				}
			}) as any;
			instance.require = requireFn;

			let result = await instance.generateEventCallback(testFunctionName);
			expect(requireFn.mock.calls).toEqual([
				// first call
				[path.join(__dirname, '..', func.webpackConfig)],
				// second call
				[bundleScriptName]
			]);
		});

		test.todo('should throw an exception when the webpack config file is missing');
		test.todo('should throw an exception when webpack returns an error');
		test.todo('should throw an exception when requiring the script throws an exception');
		test.todo('should throw an exception when the script does not call addEventListener');
	});
});
