import {Request as ExpressRequest} from "express";
import {Headers} from "node-fetch";
import {Request as CloudflareRequest} from "./runtime-apis/Request";

export function convertExpressRequestToCloudflareRequest(req: ExpressRequest): CloudflareRequest {
	const headers = new Headers();
	for (const [k, vs] of Object.entries(req.headers)) {
		for (const v of vs) {
			headers.append(k, v);
		}
	}

	return new CloudflareRequest(req.url, {
		method: req.method,
		headers,
		body: req.body,
		redirect: "manual",
		cf: {}, // TODO
	});
}
