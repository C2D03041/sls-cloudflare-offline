import {Response as ExpressResponse} from "express";
import {CloudflareResponse} from "./runtime-apis/Response";

export function applyCloudflareResponseToExpressResponse(cfr: CloudflareResponse, exr: ExpressResponse): void {
	console.log("sending res", cfr);
	if (cfr.init && cfr.init.headers) {
		for (const [k, v] of (cfr.init.headers as any).entries()) {
			exr.set(k, v);
		}
	}
	exr.status(cfr.init?.status || 200);
	exr.send(cfr.body);
}
