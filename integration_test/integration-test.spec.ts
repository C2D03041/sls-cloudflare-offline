import {chunksToLinesAsync} from "@rauschma/stringio";
import { ChildProcess, spawn } from "child_process";
import fetch from "node-fetch";
import path from "path";

const host = "127.0.0.1";
const port = "22222";
const addr = `http://${host}:${port}`;
let pid: ChildProcess = null;

beforeAll(async () => {
	const bin = path.join(__dirname, "../node_modules/.bin/serverless");
	pid = spawn(bin, ["offline", "--host", host, "-p", port], {
		env: {
			...process.env,
			SLS_DEBUG: '"*"',
		},
		cwd: __dirname,
	});

	// console.log('pid', pid);
	const lines = [];
	for await (const line of chunksToLinesAsync(pid.stdout)) {
		lines.push(line);
		if (line.includes("Offline Ready")) {
			return;
		}
	}

	// if we get down here then initialization errored out
	console.error('Initialization errors: ', lines.join(''));
}, 30000);

afterAll(async () => {
	pid.kill("SIGINT");
});

describe("ping", () => {
	it("is running", async () => {
		const r = await fetch(addr + "/api/ping");
		const body = await r.json();
		expect(r.status).toBe(200);
		expect(body).not.toBe(null);
		expect("pong" in body).toBe(true);
	});
});

describe("ping-ts", () => {
	it("is running", async () => {
		const r = await fetch(addr + "/api/ping-typescript");
		const body = await r.json();
		expect(r.status).toBe(200);
		expect(body).not.toBe(null);
		expect("pong" in body).toBe(true);
		expect("typescript" in body).toBe(true);
		expect(body.typescript).toBe(true);
	});
});
