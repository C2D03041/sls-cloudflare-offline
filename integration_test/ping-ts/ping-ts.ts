import {CallEvent} from "./../../src/runtime-apis/CallEvent";
import {Request} from "./../../src/runtime-apis/Request";

addEventListener("fetch", (event: CallEvent): void => {
	event.respondWith(helloWorld(event.request));
});

async function helloWorld(_request: Request): typeof Response {
	return new Response(JSON.stringify({ pong: Date.now(), typescript: true }));
}
