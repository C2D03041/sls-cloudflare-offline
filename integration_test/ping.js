'use strict';

addEventListener('fetch', event => {
  event.respondWith(helloWorld(event.request))
})

async function helloWorld(request) {
  return new Response(JSON.stringify({ pong: Date.now() }))
}
