Serverless Cloudflare Offline
===

Emulates Cloudflare Workers locally when developing your Serverless project. (At least, that's the goal)

This is still beta software. The CF native API bindings are far from complete; most notably Workers KV are completely unsupported.

A minimally usable example can be found in the `integration_test` subfolder.

I would recommend pulling the develop tag from npm until this library is more stable. `npm install --save-dev sls-cloudflare-offline@develop`. You should also be looking at the `develop` branch of this repository if you're using the develop tag.

How it works
---

This plugin parses your serverless.yml file for each function and bound http event. An invocation function is stored for each worker. Each worker's invocation function is separated by swapping out the underlying `global.addEventListener` function between each import. 

We then initialize our emulated CF Worker runtime API (`./src/runtime-apis/index.ts`), which exposes an API-compatible emulation of the APIs found in a real worker. This step must occur before any worker functions are invoked. These APIs are assigned to the `global` variable. This is pretty dangerous, but since this is just local development, and this is similar to what cloudflare workers do anyway, it should be mostly fine.

Then, we initialize an instance of an Express application. Express is a middleware-based http microframework. Each middleware has the signature `(request, response, next) => void`, where calling `next()` invokes the next middleware. So, we generate a middleware for each Http Event for each Worker, and push it on to the Express application instance.

Each middleware checks to see if an incoming http request matches the particular Http Event that the middleware was generated for. If the request matches, it invokes the worker callback that was generated earlier. We convert the Express request into a Cloudflare Reqeust, and then convert the Cloudflare Response back into an Express Response. If the request doesn't match, `next()` is called, which triggers the next middleware

Todo list
---

* Add support for workers KV. Ideally, this could take some optional predefined state (maybe a json file?), and just operate on a KV store in-memory. Maybe an option to persist back to disk.
* Configurable free/business/enterprise flag(s?) that toggle on/off exclusive APIs
* Configurable timeout support in line with real workers (10/50ms). At the very least, a timeout log messaage when 
* Unit test suite, and more fleshed out integration test suite
* The 404 route should allow for proxying unmatched routes to an underlying url, since this is something cloudflare supports.
* Update the integration tests to run against both the offline copy and a really deployed copy. Also ensure the response bodies from both are identical. This should help catch bugs where the runtime API diverges from real workers, but the offline test still functions properly (and vice-versa).

Contribution
---

Merges to the develop branch will automatically publish to NPM with the `develop` tag. Merges to the master branch automatically publish to NPM with the `latest` tag. If the tests pass, of course.

This automatic publishing increases the importance of unit and integration testing. Adequately tested pull requests that have a valid use case have a high probability of being merged.

Any communications that must be secured can be encrypted using the `C2D03041.gpg` public key in the root of the repository. These can be submitted as project issues or emails to `C2D03041@protonmail.com`. Don't forget to include your own pubkey if you wish to have a response.
